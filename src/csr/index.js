const mysql = require("mysql");
const express = require("express");
const path = require("path");
const cors = require("cors");
require("dotenv").config();

const PORT = process.env.PORT || 3000;

const app = express();
app.use(cors());

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

app.get("/customers", (req, res) => {
  connection.query("SELECT * FROM customers", function (
    error,
    results,
    fields
  ) {
    if (error) {
      console.error(error);
      res.status(500).send("Something went wrong");
    }
    res.json(results);
  });
});

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}`);
});
