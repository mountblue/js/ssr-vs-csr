const API_ENDPOINT = "http://localhost:3000";

function getCustomers() {
  return fetch(`${API_ENDPOINT}/customers`).then((res) => res.json());
}
function createCustomerList(customers) {
  let fragment = new DocumentFragment();
  let customersList = document.getElementById("customers");
  customers.forEach(function (customer) {
    let card = document.createElement("div");
    card.classList.add("card");
    let cardBody = document.createElement("div");
    cardBody.classList.add("card-body");
    card.appendChild(cardBody);
    let name = document.createElement("p");
    name.innerHTML = `${customer.first_name} ${customer.last_name}`;
    cardBody.appendChild(name);
    let phone = document.createElement("p");
    phone.innerHTML = customer.phone;
    cardBody.appendChild(phone);
    fragment.appendChild(card);
  });
  customersList.appendChild(fragment);
}

function main() {
  getCustomers().then(createCustomerList);
}
main();
