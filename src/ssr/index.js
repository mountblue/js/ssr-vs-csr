const mysql = require("mysql");
const express = require("express");
const path = require("path");
require("dotenv").config();

const PORT = process.env.PORT || 3000;

const app = express();
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

app.get("/customers", (req, res) => {
  connection.query("SELECT * FROM customers", function (
    error,
    results,
    fields
  ) {
    if (error) {
      console.error(error);
      res.status(500).send("Something went wrong");
    }
    res.render("customers", {
      title: "Customers",
      customers: results,
    });
  });
});

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}`);
});
